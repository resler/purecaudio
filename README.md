# README #

A simple project setup to use portaudio and libpd.  This is a great way to practice audio programming using the command line tools.  Since this project was compiled on Mac OS X, you may need to download portaudio and libpd and compile them separately and add them to your project.  

libpd lives here: http://libpd.cc/
portaudio lives here: http://www.portaudio.com