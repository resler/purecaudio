#include <stdio.h>
#include <errno.h>
#include <libproc.h>
#include <unistd.h>
#include "z_libpd.h"
#include <math.h>
#include "portaudio.h"
#define SAMPLE_RATE   (44100)
#define NUM_CHANNELS (2)

typedef struct
{
    double inbuf[64];// one input channel, two output channels
    // block size 64, one tick per buffer
    double outbuf[128]; //stereo outputs are interlaced, s[0] = RIGHT, s[1] = LEFT, etc..
    
}
paTestData;

//globals
static paTestData data;
float fooFloat = 0;

/* This routine will be called by the PortAudio engine when audio is needed.
 ** It may called at interrupt level on some machines so don't do anything
 ** that could mess up the system like calling malloc() or free().
 */
static int patestCallback( const void *inputBuffer, void *outputBuffer,
                          unsigned long framesPerBuffer,
                          const PaStreamCallbackTimeInfo* timeInfo,
                          PaStreamCallbackFlags statusFlags,
                          void *userData )
{
    /* Cast data passed through stream to our structure. */
    paTestData *data = (paTestData*)userData;
    float *out = (float*)outputBuffer;
    unsigned int i;
    
    (void) inputBuffer; /* Prevent unused variable warning. */
    
    libpd_process_double(1, data->inbuf, data->outbuf);
    
    //dsp perform routine
    for( i=0; i<framesPerBuffer*NUM_CHANNELS; i++ )
    {
        if(i % 2)
            *out++ = data->outbuf[i]; // right channel
        else
            *out++ = data->outbuf[i]; // left channel
    }
    
    return 0;
}

//query the path of th executable using the Process ID (PID),
//UNIX only, for Windows us _fullpath() or other API
char *getPath() {
    int ret;
    pid_t pid;
    char pathbuf[PROC_PIDPATHINFO_MAXSIZE];
    
    pid = getpid();
    ret = proc_pidpath(pid, pathbuf, sizeof(pathbuf));
    if ( ret <= 0 ) {
        fprintf(stderr, "PID %d: proc_pidpath ();\n", pid);
        fprintf(stderr, "    %s\n", strerror(errno));
    } else {
        printf("proc %d: %s\n", pid, pathbuf);
    }
    
    return pathbuf;
}

//use with libpd_printhook to print to console
void pdprint(const char *s) {
    printf("libpd print: %s", s);
}

//use with libpd_noteonhook to print to console
void pdnoteon(int ch, int pitch, int vel) {
    printf("noteon: %d %d %d\n", ch, pitch, vel);
}

//use with libpd_floathook to get data from [s foo]
void pdfloat(const char *source, float f) {
    
    printf("(%s): %f\n", source, f);
    fooFloat = f;
}


int main(int argc, char **argv) {
    
    printf("PortAudio Test: with libpd.\n");
    char *path = getPath(); // if you want to get the path of your executable to locate your patch below.
    // init pd
    int srate = SAMPLE_RATE;
    int blcksize = libpd_blocksize();
    
    libpd_set_printhook(pdprint);
    //libpd_set_noteonhook(pdnoteon);
    
    libpd_set_floathook(pdfloat);
    libpd_init();
    libpd_init_audio(1, 2, srate); //one channel in, one channel out
    
    
    // compute audio    [; pd dsp 1(
    libpd_start_message(1); // one entry in list
    libpd_add_float(1.0f);
    libpd_finish_message("pd", "dsp");
    libpd_bind("foo");
    
    // open patch       [; pd open file folder(
    void* handle;
    handle = libpd_openfile("test.pd","./");
    //you could use the getPath() method for the folder location
    //otherwise put the path name as the second argument
    
    PaStream *stream; //opens the audio stream
    PaError err;
    
    libpd_float("myMessage", 11001);
    
    /* Initialize our data for use by callback. */
    for (int i = 0; i < blcksize; i++)
        data.outbuf[i] = 0;
    
    /* Initialize library before making any other calls. */
    err = Pa_Initialize();
    if( err != paNoError ) goto error;
    
    /* Open an audio I/O stream. */
    err = Pa_OpenDefaultStream( &stream,
                               1,          /* input channels */
                               2,          /* output channels */
                               paFloat32,  /* 32 bit floating point output */
                               SAMPLE_RATE,
                               (long)blcksize,        /* frames per buffer */
                               patestCallback,
                               &data );
    if( err != paNoError ) goto error;
    
    err = Pa_StartStream( stream );
    if( err != paNoError ) goto error;
    
    Pa_Sleep(4000); // sleeps for 4 seconds, then ends.
    
    
    err = Pa_StopStream( stream );
    if( err != paNoError ) goto error;
    err = Pa_CloseStream( stream );
    if( err != paNoError ) goto error;
    Pa_Terminate();
    
    return err;
    
error:
    Pa_Terminate();
    fprintf( stderr, "An error occured while using the portaudio stream\n" );
    fprintf( stderr, "Error number: %d\n", err );
    fprintf( stderr, "Error message: %s\n", Pa_GetErrorText( err ) );
    
    libpd_closefile(handle);
    return 0;
}
